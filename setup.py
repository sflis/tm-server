from setuptools import setup, find_packages

PACKAGENAME = "tm-standalone-ctrl"
DESCRIPTION = "Target Module control server for communicating with TMs"
AUTHOR = "Samuel Flis"
AUTHOR_EMAIL = "samuel.d.flis@gmail.com"
VERSION = "0.1.0"

setup(
    name=PACKAGENAME,
    packages=find_packages(),
    version=VERSION,
    description=DESCRIPTION,
    provides=["tm_server"],
    license='BSD3',
    install_requires=[
        'pyyaml',
        'numpy',
        'matplotlib',
        'tqdm',
        'pandas>=0.21.0',
        'pyqtgraph',
        'numba',
        'zmq',
        'pyqt5'
    ],
    # setup_requires=['pytest-runner', ],
    # tests_require=['pytest', ],
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    package_data={
        '': ['data/*'],
    },
    entry_points={'console_scripts':
                    [
                    'tm_start_serv=tm_server.scripts.start_tm_server:main',
                    'tm_mon=tm_server.scripts.monitor:main',
                    'tm_log_listener=tm_server.server.log_server:log_client',
                    'tm_ctrl_shell=tm_server.scripts.tm_command_shell:main',
                    ]
                    }
)