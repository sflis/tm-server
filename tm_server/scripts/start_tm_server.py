from tm_server.server.tm_server import TMServer
import yaml


def main():    
    import sys
    configuration_file = sys.argv[1]
    config = yaml.load(open(configuration_file,'r'))
    s = TMServer(config)
    s.run()

if(__name__ == "__main__"):
    main()