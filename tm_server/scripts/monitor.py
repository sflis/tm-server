from tm_server.tools.MonitorVis import DynamicPlotter

def main():
    plt = DynamicPlotter()
    plt.run()  # Start plotting

if(__name__ == '__main__'):
    main()