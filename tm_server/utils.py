import datetime

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    colrmap = {'v':HEADER,'b':OKBLUE,'g':OKGREEN,'y':WARNING,'r':FAIL,'f':BOLD,'u':UNDERLINE,'e':ENDC}

class msg:
    
    @staticmethod
    def colr(fmt,s):
        if(len(fmt)>1):
            return "".join([bcolors.colrmap[k] for k in fmt])+s+bcolors.colrmap['e']
        else:
            return bcolors.colrmap[fmt]+s+bcolors.colrmap['e']
    
    @staticmethod
    def warn(s):
        return bcolors.WARNING+bcolors.BOLD+"Warning:"+bcolors.ENDC+bcolors.WARNING+" %s"%s+bcolors.ENDC
    
    @staticmethod
    def err(s):
        print(bcolors.FAIL+"Error: %s"%s+bcolors.ENDC)

def recv_child_com(cv,daq):
    import sys
    with cv:
        #if run still set to active in the DAQ
        #the run has terminated itself 
        if(daq.run_active):
            # print(daq.cmdqueue)
            # daq.cmdqueue.append('\n')
            daq._stop(arg='Finnished')


import time
def hearbeat_send(interval,con):
    while(True):
        time.sleep(interval)
        con.send('1')

def heartbeat_recv(interval,con,fail_func):
    while(True):
        try:
            con.poll(timeout=interval*2)
        except:
            fail_func()
            break
def conv_string_date(dstr):
    return datetime.datetime.strptime(dstr,'%Y-%m-%d %H:%M:%S.%f')