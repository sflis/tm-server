import sqlite3 as lite
import os
import numpy as np
from numpy.random import uniform as ru
import time
import datetime
from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
import pyqtgraph as pg
import sqlite3 
#QtGui.QApplication.setGraphicsSystem('raster')

import pyqtgraph as pg
import random
import time
import numpy as np
import collections
import socket
import threading
import math
from pyqtgraph.Qt import QtGui, QtCore
from datetime import datetime

color_set1 = [(228,26,28),
              (55,126,184),
                (77,175,74),
                (152,78,163),
                (255,127,0),
                (255,255,51),
                (166,86,40),
                (247,129,191),
                (153,153,153)]
color_set2 = [(251,180,174),
                (179,205,227),
                (204,235,197),
                (222,203,228),
                (254,217,166),
                (255,255,204),
                (229,216,189),
                (253,218,236),
                (242,242,242)]

color_set3 = [(166,206,227),
                (31,120,180),
                (178,223,138),
                (51,160,44),
                (251,154,153),
                (227,26,28),
                (253,191,111),
                (255,127,0),
                (202,178,214),
                (106,61,154),
                (255,255,153),
                (177,89,40)]
class DynamicPlotter():
    def __init__(self, sampleinterval=2, timewindow=10., size=(600,350),tm=None,pixel_mapping=None):
        # PyQtGraph stuff
        self._interval = int(sampleinterval*1000)
        self.app = QtGui.QApplication([])
        self.win = pg.GraphicsWindow()
        self.win.setWindowTitle('Standalone Target Module Monitoring')
        #data 
        path ='./'# os.path.dirname(os.path.abspath(__file__))   
        self.con = lite.connect(path+'/monitor.db', check_same_thread=False,detect_types=sqlite3.PARSE_DECLTYPES) 
        cur = self.con.execute('select * from TMMonitor')
        self.col_name_index = dict()
        for i, c in enumerate(cur.description):
            self.col_name_index[c[0]] = i
        self.data = {'hv_current':list(),'hv_voltage':list()}
        self.time = list()
        self.plts = {}
        self.curve_names = list()
        self.plot_time_window = 10
        # self.win.addItem(pg.TextItem(text='HEJHEJHEJ', color=(200, 200, 200), html=None, anchor=(0, 0), border=None, fill=None, angle=0, rotateAxis=None),row=1 )
        self._add_plot('HV current',('Current','A'),('Time','min'),['hv_current'])       
        self._add_plot('HV Voltage',('Voltage','V'),('Time','min'),['hv_voltage'])
        # self._add_plot('DAC HV Voltage (Super pixels 0-8)',('Voltage','V'),('Time','min'),['dac_suppix_%d'%i for i in range(9)])
        self.win.nextRow()
        # self._add_plot('DAC HV Voltage (Super pixels 9-15)',('Voltage','V'),('Time','min'),['dac_suppix_%d'%i for i in range(9,16)])

        self._add_plot('Temperature',('Temperature',u"\u00B0"+'C'),('Time','min'),['temp_powb','temp_auxb','temp_primb'])#,'temp_sipm'
        
        self.img = pg.ImageItem(levels=(0,400))
        self.p = self.win.addPlot()
        self.p.addItem(self.img)

         # Add a color scale
        # self.gl = pg.GradientLegend((20, 150), (-10, -10))
        # self.gl.setParentItem(self.img)
        # self.gl.scale(1,-1)
        # self.gl
        # self.colorScale.setLabels('Label')
        #self.p.scene().addItem(self.colorScale)
        # QTimer
        
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.updateplots)
        self.timer.start(self._interval)
        from ..server import distributor_server
        #Target Module connection
        self.tm=distributor_server.AcquireTM()
        print(self.tm.connected)
        self.dead_pixels=np.array([0]*32+[1]*32)
        self.dead_pixels[57] = 0

        self.pixel_mapping = {}
        if(pixel_mapping !=None):
            for k,v in pixel_mapping.items():
                self.pixel_mapping[v-1] = k-1
        
        if(self.tm != None):
            self.tm.WriteSetting("SlowADCEnable_Primary", 1)
            self.tm.WriteSetting("SlowADCEnable_Aux", 1)

    def _add_plot(self,title,labely,labelx,curves):
        self.curve_names += curves
        self.plts[title] = (self.win.addPlot(title=title),curves,list())
        self.plts[title][0].setLabel('left', labely[0], labely[1])
        self.plts[title][0].setLabel('bottom', labelx[0], labelx[1])
        self.plts[title][0].addLegend()
        self.plts[title][0].showGrid(x=True, y=True)
        for i,curve in enumerate(curves):
            t = sum((color_set1[i],(20,)),())
            self.plts[title][2].append(
                                        self.plts[title][0].plot(
                                                                np.linspace(0,10,10), 
                                                                np.linspace(0,10,10),
                                                                pen=color_set1[i],
                                                                fillLevel=0,
                                                                fillBrush = t,
                                                                name=curve))
        self.plts[title][0].setXRange(-self.plot_time_window,0)
            # self.plts[title][2][i].setClickable() 

    def get_data(self):
        with self.con:
            cur = self.con.cursor()
            cur.execute("SELECT * FROM TMMonitor")
            rows = cur.fetchall()
            for k in self.curve_names: 
                self.data[k] = list()
            self.time = list()
        now = datetime.now()
        first_time = rows[0][1]
        for row in rows:
            for c in self.curve_names:
                self.data[c].append(row[self.col_name_index[c]])
            self.time.append(first_time-row[1])
        if(self.tm==None):
            data = np.random.uniform(0,2,(8,8))
        else:
            data = self.tm.readSlowSignalMonitor()#*self.dead_pixels
            # d = data
            data[self.dead_pixels==0] = 0#-0.001
            d = np.zeros(8*8)
            for k,v in self.pixel_mapping.items():
                d[v] = data[k]
            d = d.reshape((8,8))
        self.img.setImage(np.rot90(np.rot90(np.rot90(d))))    

    def run(self):
        self.app.exec_()
    

    def _update_plot(self,time,plot):
        for i in range(len(plot[2])):
            plot[2][i].setData(time,self.data[plot[1][i]][::-1],fillLevel=0.5)
            
    
    def updateplots(self):
        self.get_data()
        time = list()
        for t in self.time:
            time.append(t.total_seconds()/60)
        t = np.array(time)
        trange = t[t>t[-1]-self.plot_time_window]
        for k,v in self.plts.items():
            # v[0].setXRange(trange[0], trange[-1])
            self._update_plot(time,v)
        
        self.app.processEvents()