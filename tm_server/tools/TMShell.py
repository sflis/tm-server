import cmd
import yaml
from multiprocessing import Process, Manager
import threading
import datetime
import time
import os
import sqlite3 as lite
import pickle

#for python2 compatibility
try:
   input = raw_input
except NameError:
   pass

from tm_server.utils import *
from tm_server.target_module.tm_wrapper import *
# from run_definitions import *
from tm_server.server import distributor_server

class TMShell(cmd.Cmd):
    intro = 'Welcome to TMShell command shell.    Type help or ? to list commands.\nPrompt color explanation: \n    '\
    +bcolors.OKBLUE + '(SSM-DAQ) '+bcolors.ENDC +' : No data taking \n    '\
    +bcolors.FAIL +bcolors.BOLD + '(SSM-DAQ*) '+bcolors.ENDC+': Data taking is active'
    
    prompt = bcolors.OKBLUE + '(TMShell) '+bcolors.ENDC
    file = None
    def __init__(self,argv):
        cmd.Cmd.__init__(self)
        configuration_file = argv[1]
        self.config = yaml.load(open(configuration_file,'r'))
        self.connected = False
        self.configured = False
        self.run_active = False
        self.active_run = None
        self.sim = False
        self.manager = Manager()
        self.running = self.manager.Value(int,0)
        self.is_monitoring = False
        self.tm = None
    def join_thread(self):
        self.recv_thread.join()

    def preloop(self):
        #Connect to database
        path = os.path.dirname(os.path.abspath(__file__))
        self.con = lite.connect(path+'/runs.db', check_same_thread=False)
        with self.con:
            cur = self.con.cursor()
            cur.execute("SELECT name FROM sqlite_master WHERE type='table';")

            l = cur.fetchall()
            #check if the Runs table is in the database
            #otherwise create it
            if(len(l)==0 or 'Runs' not in l[0]):
                cur.execute(''' CREATE TABLE Runs(RunNumber INTEGER PRIMARY KEY, 
                                                    Type TEXT, 
                                                    DataPath TEXT, 
                                                    StartTime TEXT, 
                                                    StopTime TEXT, 
                                                    Config BLOB,
                                                    Meta BLOB, 
                                                    SampFreq REAL, 
                                                    Status TEXT)'''
                                                    )
        
    def do_start(self,arg):
        'Start data taking'

        from copy import deepcopy
        config = deepcopy(self.config['run_configuration'])
        if(self.sim):
            ext='.txt'
            target = sim_data_taking
            arg_dict = {}
            config['run_type'] +='SIM'
        else:
            if(not self.connected):
                print(bcolors.FAIL+'Error: Could not start run: not connected to a Target Module'+bcolors.ENDC)
                return
            arg_dict = {'tm':self.tm}
            ext='.hdf5'
            target = eval(config['run_function'])#ssm_data_taking
        self.prepare_run(config)
        if(not self.prepared):
            print(bcolors.FAIL+'Error: Could not start run: run preparation failed'+bcolors.ENDC)
            return
        if('meta_info' in config.keys()):
            meta_info = config['meta_info']
        else:
            meta_info = {'pre_comment':'','post_comment':''}
        if(len(arg)>0):
            meta_info['pre_comment'] = arg
        print(meta_info)
        with self.con:
            cur = self.con.cursor()
            sqlite_str = '''INSERT INTO 
                            Runs(Type,DataPath,StartTime,StopTime,Config,Meta,SampFreq,Status) 
                            VALUES ('%s','%s','%s','--:--:--','%s','%s','%f','%s');'''%(config['run_type'],
                                                                                    config['data_path'],
                                                                                    '',
                                                                                    yaml.dump(self.config),
                                                                                    yaml.dump(meta_info),
                                                                                    config['sampl_freq'],
                                                                                    'Running'
                                                                                    )
            cur.execute(sqlite_str)
            lid = cur.lastrowid
        print('Starting run %.5d'%lid)

        self.running.value=1
        self.condition = self.manager.Condition()
        
        
        #start run process
        start_time = datetime.datetime.now()
        file_name =os.path.join(config['data_path'],'Run%.5d'%(lid)+ext)
        self.run_process = Process(target=target, args=(self.running,file_name ,self.condition,config,arg_dict))
        self.run_process.start()
        self.run_active = True
        
        #wait and start comm thread
        time.sleep(0.5)
        self.recv_thread = threading.Thread(name='end com', target = recv_child_com, args=(self.condition,self) )
        self.recv_thread.start()
       
        #Setting prompt to running mode
        self.prompt = bcolors.FAIL +bcolors.BOLD + '(SSM-DAQ*) '+bcolors.ENDC
        
        #save meta information
        meta_info_file = os.path.join(config['data_path'],'Run%.5d-meta'%(lid)+'.pkl')
        print(meta_info_file)
        pickle.dump({'Type': config['run_type'],'StartTime':start_time,'RunNumber':lid,'meta_info':meta_info},
            open(meta_info_file,'wb'),)

        self.active_run = {'Type': config['run_type'],'StartTime':start_time,'RunNumber':lid,'meta_info':meta_info}
        #Updating database with start time
        with self.con:
            cur = self.con.cursor()
            cur.execute("UPDATE Runs SET StartTime = '%s' WHERE RunNumber = %d;"%(str(start_time),lid))
        if(self.sim):
            config['run_type']=config['run_type'][:-3]
    
    def prepare_run(self, config):
        self.prepared = False
        if('data_folder' not in config.keys()):
            print(bcolors.FAIL+'Error: Can not prepare run: no `data_folder` key in configuration'+bcolors.ENDC)
            return
        root_data_folder = config['data_folder']
        if not os.path.exists(root_data_folder):
            print(bcolors.FAIL+'Error: Can not prepare run: configure data folder does not exist'+bcolors.ENDC)
            return
        
        data_subfolder = 'd%s_%s'%(datetime.datetime.now().date(),config['run_type'])
        full_path = os.path.join(root_data_folder,data_subfolder)
        if not os.path.exists(full_path):
            print("Creating the following directory for data files:\n `%s`"%full_path)
            os.makedirs(full_path)
        config['data_path'] = full_path
        self.prepared = True

    def do_status(self, arg):
        'Display current DAQ status'
        if(self.tm.connected):
            status = self.tm.get_tm_status()
        else:
            status = {}
        if(status == None):
            status = {}
        def pr_stat(object,status):
            print('%-25s %s'%('%s :'%object,status))
        def yon(bool):
            return msg.colr('gf','YES') if bool else msg.colr('rf','NO')
        
        pr_stat('Target Module connected',yon(self.tm.connected))
        pr_stat('Run active',yon(self.run_active))
        for k,v in status.items():
                if(isinstance(v,bool)):
                    pr_stat('TM %s'%(k),yon(v))
                else:
                    pr_stat('TM %s'%(k),str(v))
        if(self.run_active):
            pr_stat('Run number','%d'%self.active_run['RunNumber'])
            pr_stat('Run duration','%s'%(datetime.datetime.now()-self.active_run['StartTime']))
            pr_stat('Samples collected','%d'%(10))
        else:
            pr_stat('Run number','--')
            pr_stat('Run duration','--:--:--')
            pr_stat('Samples collected','--')

    def do_stop(self, arg):
        'Stop current data taking run'
        self._stop('User')

    def _stop(self, arg): 
        'Stop data taking run'
        import signal
        if(self.run_active):
            self.running.value=0
            self.run_active = False
            self.run_process.join()
            stop_time = datetime.datetime.now()
            exitcode = self.run_process.exitcode
            if(exitcode == 0):
                status = 'Stopped'
                if(arg == "Finnished"):
                    status = 'Success'
            else:
                status = 'Failed'
            print(status)
            self.prompt = bcolors.OKBLUE + '(SSM-DAQ) '+bcolors.ENDC
            sys.stdout.write('\n'+self.prompt)
            sys.stdout.flush()

            #Update database with post run info
            with self.con:

                cur = self.con.cursor()
                cur.execute('SELECT max(RunNumber) FROM Runs')
                lid= cur.fetchone()[0]
                print('\nStopped run %.5d'%lid)
                print('please write a post run comment: ')
                post_comment = input()
                meta = self.active_run['meta_info']
                meta['post_comment'] = post_comment 
                cur.execute("UPDATE Runs SET StopTime = '%s', Status = '%s', Meta = '%s' WHERE RunNumber = %d;"%(str(stop_time),status,yaml.dump(meta),lid))
            
        else:
            print('No active run to stop!')

    def do_list_runs(self,arg):
        'List previous runs in database'
        with self.con:
            cur = self.con.cursor()
            cur.execute("SELECT * FROM Runs")
            rows = cur.fetchall()
        print(bcolors.HEADER+ bcolors.BOLD+'%10s  %-20s  %-20s  %15s  %10s '%('Run number','Type','StartTime','Duration','Status') +bcolors.ENDC)
        for row in rows:
            if('--' not in row[4]):
                t_start = conv_string_date(row[3])
                t_end = conv_string_date(row[4])
                dt = t_end-t_start
            else:
                dt = '0:00:00.000000'
            
            print('%10.5d  %-20s  %-20s  %15s  %10s'%(row[0],row[1],row[3][:-7],dt,row[8]))

    def do_tm(self,arg):
        'Pass commands directly to the Target Module'
        if(len(arg)==0):
            arg = 'help'
        self.tm.command(arg)
    
    def complete_tm(self, text, line, begidx, endidx):
        return self.tm._complete(text,line,begidx,endidx)
        

    def do_connect_tm(self, arg):
        'Connect to Target module'
        self.tm = distributor_server.AcquireTM()#TargetModuleWrapper(**self.config['tm_config'])
        if (self.tm.connected):
            self.connected = True
        else:
            msg.warn('Not connected to Target Module')

    def do_enable_bias_voltage(self,arg):
        '''Enables bias voltage to the SiPMs using 
           configuration settings in `sipm_bias_voltage`
        '''
        for k,v in self.config['sipm_bias_voltage'].items():
            self.tm.SetHVDAC(k,v,True)

    def do_configuration(self,arg):
        'Prints out the loaded configuration'
        if(arg=='keys'):
            print(self.config.keys())
        else:
            print(yaml.dump(self.config))
        
    def do_configure(self, arg):
        'Configure or reconfigure a setting. Arg: <setting name>\n'\
        'For nested configurations the syntax is <setting name>.<sub-setting>\n'\
        'Example:\n'\
        '     (SSM-DAQ) configure tm_config.host_port\n'\
        '     8201\n'\
        '     Field tm_config.host_port is set to 8201\n'\
        '     (SSM-DAQ)'

        try:
            inp = input()
            val = eval(inp)
        except:
            val = inp
        
        if(isinstance(self.config[arg.split('.')[0]],dict)):
            argsp = arg.split('.')
            self.config[argsp[0]][argsp[1]] = val
        else:    
            self.config[arg] = val
        
        print("Field `%s` is set to: `%s`"%(arg,str(val)))
    
    def do_sim(self, arg):
        'Sets simulation mode (for development)'
        self.sim = self.sim != True
        print('Simulation mode set to: `%s`'%self.sim)
    
    def do_load_config(self,arg):
        'Load a new configuration file'
        self.config = yaml.load(open(arg,'r'))

    # def do_monitor(self,arg):
    #     'Starts the monitoring daemon and monitor panel'
    #     from monitor import monitor_panel_daemon
    #     if(self.connected ):
    #         if(not self.is_monitoring):
    #             th = threading.Thread(target=monitor_panel_daemon,
    #                       kwargs={'tm': self.tm,'plotter_kwargs':{'pixel_mapping':self.config['sensor_mapping']}})

    #             th.daemon = True
    #             th.start() # Start thread
    #             self.is_monitoring = True
    #         else:
    #             msg.warn('Monitoring already enabled')

    def do_exit(self, arg):
        'Exit TMShell command shell'
        if(self.run_active):
            ans = input(msg.warn('A data run is currently active. \nDo you want to exit and terminate the run anyway? (yes or no) '))
            if(ans == 'no'): 
                return
            while(ans!= 'yes'):
                ans = input('please type "yes" or "no"')
                if(ans == 'no'): 
                    return
        if(self.tm !=None):
            self.tm.close()
            self.tm.tm.CloseSockets()
        if(self.run_active):
            self.do_stop(arg)
            self.tm.tm.CloseSockets()
        return True
    def emptyline(self):
        pass

    def do_shell(self, args):
        """Pass command to a system shell when line begins with '!'"""
        os.system(args)
    
    do_EOF = do_exit

