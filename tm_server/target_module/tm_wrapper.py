import pandas as pd
import numpy as np
import datetime
import time
from tm_server.utils import *
import sys
from tm_server.server.log_server import Logger
import sys
def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

import inspect
def rh(res):
    if(isinstance(res,list)):
        r = res[0]
    else:
        r = res
    if(r != 0):
        eprint(msg.colr('rf','TC Error code: %d '%r)+' Called from `%s` '%inspect.stack()[1][3])

    return res

import multiprocessing
from multiprocessing import Process, Lock
from functools import wraps
import threading

TM_ALIVE = True

def hb_listener(socket_name, hb_socket,tm):
    global TM_ALIVE
    import zmq
    
    hb_socket.setsockopt(zmq.SUBSCRIBE, b'')
    hb_socket.setsockopt( zmq.LINGER, 0 )
    hb_socket.connect(socket_name)
    try:
        while(True):
            msg = hb_socket.recv_pyobj()
            if(msg=="ALIVE"):
                TM_ALIVE = True
            elif(msg=='DEAD'):
                TM_ALIVE = False
            else:
                TM_ALIVE = False

            if(not tm.running ):
                break
    except KeyboardInterrupt:
        hb_socket.close()
        print('exiting')
        return
    hb_socket.close()

import os        
class TMWrapper(object):
    tm_counter = []
    running = True
    def __init__(self,
                    tm_config_path,
                    firmware_def_file,
                    asic_def_file,
                    trigg_asic_def_file, 
                    host_ip, 
                    tm_ip, 
                    host_port, 
                    tm_port, 
                    init,
                    sync_variables = None,
                    reconnect=False):
        print(os.getpid())
        if(os.getpid() in TMWrapper.tm_counter):

            print("ERROR Already an instance of the TM wrapper in this process ",os.getpid() )
            print(TMWrapper.tm_counter)
            raise RuntimeError
        TMWrapper.tm_counter.append(os.getpid())
        self.log = Logger(
            name='Target module',
            print=True)
        self.log('Acquering lock')
        
        self.listen_to_heartbeat = False
        self.connection_valid = True
        if(sync_variables==None):
            self.m = multiprocessing.Manager()
            self.lock = self.m.Lock()
            self.connected = self.m.Value('i',0)
        else:
            self.lock = sync_variables[0]
            if(len(sync_variables)==2):
                import zmq
                self.context = zmq.Context()
                self.hb_socket = self.context.socket(zmq.SUB)
                self.hb_listener_th = threading.Thread(target = hb_listener, 
                                                    args = (sync_variables[1], self.hb_socket,self))
                self.hb_listener_th.start()
                self.listen_to_heartbeat = True
        # self.connected = False
        imported_target = False
        stat = 1

        try:
            from target_driver import TargetModule
            from os.path import join 
            self.tm = TargetModule( join(tm_config_path,firmware_def_file),
                                        join(tm_config_path,asic_def_file),
                                        join(tm_config_path,trigg_asic_def_file))
            imported_target = True
        except:
            self.log('Could not import target_driver...')
            self.log('Limited features available...')
                
        if(imported_target):
            if(not reconnect ):
                self.log('Connecting to ip: %s'%tm_ip)
                stat = self.tm.EstablishSlowControlLink(host_ip, tm_ip)
                self.prepared_ssm = False
                
                if(stat==0):
                    self.log("Connection OK")
                    # self.connected = True
                    self.log('Executing TM initializing procedure from configuration...')
                    for proc in init:
                        if(proc[0] == 'func'):
                            eval(proc[1])
                            self.log('\tExecuted: `%s` ...'%proc[1])
                        elif(proc[0] == 'setting'):
                            self.WriteSetting(proc[1],proc[2])
                            self.log('\tSetting: `%s` set to 0x%02x'%(proc[1],proc[2]))

            else:
                stat = self.tm.ReconnectToServer(host_ip,
                                                host_port,
                                                tm_ip,
                                                8105, #tm_port,
                                                10000)
                
        
        if(stat != 0):
            self.log('Connection Failed')
            TM_ALIVE = False
            self.connection_valid = False
            TMWrapper.tm_counter.remove(os.getpid())
            # self.connected = False
        # else:
            # self.connected = True
        self.log.print = False
        self._init_commands()
        #import atexit
        #atexit.register(self.__exit__)

    @property
    def connected(self):
        global TM_ALIVE

        return TM_ALIVE and self.connection_valid 
        # return self.__connected
    
    def _init_commands(self):
        
        self.commands = {'write_setting':self.WriteSetting,
                         'enable_hv_all':self.EnableHVAll,
                         'disable_hv_all':self.DisableHVAll,
                         'initialise':self.Initialise,
                         'enable_dll_feedback':self.EnableDLLFeedback,
                         'prepare_fsd':self.PrepareFastSignalDataTaking,
                         'power_up_asic':self.PowerUpASIC,
                         'help':self._help,
                         'print':self._print,
                         'sethvdac':self.SetHVDAC,
                         'disable_hv_sp':self.DisableHVSuperPixel,
                         'verbose':self.Verbose,
                         'prepare_ssm':self.PrepareSlowSignalDataTaking,
                         'go_safe':self.GoToSafe,
                         'go_ready':self.GoToReady,
                         'go_presync':self.GoToPreSync
                        }
        
        self.print_commands = {'fw_version':self.GetFirmwareVersion,
                                'monitor_vals':self.monitor,
                                'status':self.get_tm_status,
                                'read_setting':self.ReadSetting,
                                'read_hvdac':self.ReadHVDAC,
                                'which_ch_enabled':self.WhichEnabled,
                                'ssm_data':self.readSlowSignalMonitor
                                }
        
    def close(self):
        TMWrapper.running = False
        self.hb_listener_th.join()
        self.context.term()
    
    def __del__(self):
        # import os
        # if(os.getpid() in TMWrapper.tm_counter):    
        #     TMWrapper.tm_counter.remove(os.getpid())
        if(self.listen_to_heartbeat):
            self.hb_socket.close()
            self.context.term()
            self.hb_listener_th.join()

    def _help(self, args=None):
        'Prints out the help message ('
        if(args==None):
            print('The following Target Module commands are available (type help <topic>):')
            print('=======================================================================')
            for k,c in self.commands.items():
                print('   '+k) 
        else:
            if(args[0] in self.commands.keys()):
               print(self.commands[args[0]].__doc__)
            else:
                print('*** no help on %s'%args[0])

    def _complete(self, text, line, begidx, endidx):
        auto_list = [i for i in self.commands.keys() if i.startswith(text)]
        lsplit = line[:begidx].split()
        if(len(lsplit)==2 and lsplit[1] == 'print'):
            auto_list = [i for i in self.print_commands.keys() if i.startswith(text)]
        return auto_list 

    def command(self,arg):
        argl = arg.split()
        try:
            if(len(argl)>1):
                if(self.commands[argl[0]].__name__[0]=='_'):
                    self.commands[argl[0]](argl[1:])
                else:
                    args =[eval(inp) for inp in argl[1:]]
                    self.commands[argl[0]](*args)
            else:
                self.commands[argl[0]]()
        except ValueError:
            print("*** wrong arguments")
        # except TypeError:
        #     print("*** wrong number of arguments")
        except KeyError:
            print("*** no such command: `%s"%argl[0])
    
    def _log(func):
        @wraps(func)
        def connected(*args, **kwargs):
            args[0].log(" calling: %s"%func.__name__)
            r = func(*args, **kwargs)
            return r
        return connected
        

    def _connected(func):
        @wraps(func)
        def connected(*args, **kwargs):
            global TM_ALIVE
            if('_lock' not in kwargs.keys()):
                args[0].log(" calling -> %s"%func.__name__)
            # print(args[0].connected,TM_ALIVE)
            if(args[0].connected and TM_ALIVE):
                r = func(*args, **kwargs)
            else:
                print('Not connected')
                return None
            return r
        return connected
    
    def _locked(func):
        @wraps(func)
        def locked(*args,_lock = True, **kwargs ):
            if(_lock):
                with args[0].lock:
                    r = func(*args, **kwargs)
            else:
                r = func(*args, **kwargs)
            return r
        return locked

    def _print(self,argl):
        print(argl)
        try:
            if(len(argl)>1):
                args =[eval(inp) for inp in argl[1:]]
                print(self.print_commands[argl[0]](*args))
                 # print(self.print_commands[argl[0]](argl[1:])[1])
            else:
                if(argl[0]=='fw_version'):
                    print(hex(self.print_commands[argl[0]]()[1]))
                else:
                    print(self.print_commands[argl[0]]())


        except ValueError:
            print ("*** wrong arguments")    
        except KeyError:
            print("*** no such command to print: `%s"%argl[0])
    
    @_connected
    @_locked
    def monitor(self):
        mon_vals = {}
        mon_vals['hv_current'] = self.tm.ReadHVCurrentInput()[1]
        mon_vals['hv_voltage'] = self.tm.ReadHVVoltageInput()[1]
        if(mon_vals['hv_current']==0.08190000057220459):
            mon_vals['hv_current'] = 0
        if(mon_vals['hv_voltage']==102.375):
            mon_vals['hv_voltage'] = 0
        mon_vals['temp_powb'] = self.tm.GetTempI2CPower()[1]
        mon_vals['temp_auxb'] = self.tm.GetTempI2CAux()[1]
        mon_vals['temp_primb'] = self.tm.GetTempI2CPrimary()[1]
        mon_vals['temp_sipm'] = self.tm.GetTempSIPM()[1]
        if(self.connected):
            dacvals = self.ReadHVDAC(_lock=False)
            for k,v in dacvals.items():
                    mon_vals['dac_suppix_%d'%k] = v
        return mon_vals
    
    @_connected
    @_locked
    def WriteSetting(self,setting_name,val):
        'Writes a setting to the TM'
        return rh(self.tm.WriteSetting(setting_name,val))
    
    @_connected
    @_locked
    def ReadSetting(self, setting_name):
        'Reads a register on the TM using a `setting_name`'
        print(setting_name)
        return rh(self.tm.ReadSetting(setting_name))

    
    @_connected
    @_locked
    def readSlowSignalMonitor(self):
        '''Performs slow signal monitor readout and returns a numpy array with 
           the results. By convention elements 0-31 are readouts from the 
           primary board and 32-64 from the auxilary board.  Note that the 
           readout maps to readout lines and not to pixel numbers.
        '''
        data = ''
        data = np.zeros(64)
        if(not self.connected or not TM_ALIVE): # no real TARGET module selected
            for i in range(2):
                for j in range(32):
                    data[channel] = np.random.uniform(0,1)
        else:
            # channels on primary board
            for channel in range(32):
                SettingName = "SlowResult%d_Primary" % channel
                
                answer = rh(self.tm.ReadSetting(SettingName))
        
                #0x0 is 2.5 V
                #0x7FFF is 5.0 V
                #0x8000 is 0 V
                #0xFFFF is 2.5 V
                if answer[1] < 0x8000:
                    answer[1] = answer[1] + 0x8000
                else:
                    answer[1] = answer[1] & 0x7FFF
        
                data[channel] = float(answer[1])* 0.03815*2.
       
            # channels on auxiliary board
            for channel in range(32):
                SettingName = "SlowResult%d_Aux" % channel
                answer = rh(self.tm.ReadSetting(SettingName))
                  
                if answer[1] < 0x8000:
                    answer[1] = answer[1] + 0x8000
                else:
                    answer[1] = answer[1] & 0x7FFF
        
                data[channel+32] = float(answer[1])* 0.03815*2.
        return data
    
    @_connected
    @_locked
    def GetFirmwareVersion(self):
        'Returns the firmware version on the TM'
        return rh(self.tm.GetFirmwareVersion())
    
    @_connected
    @_locked
    def EnableHVAll(self):
        'Enables the HV for all channels'
        return rh(self.tm.EnableHVAll())

    @_connected
    @_locked
    def DisableHVAll(self):
        '''Disables the HV for all channels'''
        return rh(self.tm.DisableHVAll())

    @_connected
    @_locked
    def get_tm_status(self):
        status = {}
        status['state'] = rh(self.tm.GetState())
        enabled_dacs = self.WhichEnabled(_lock=False)
        for k,v in enabled_dacs.items():
            status['Enabled DAC ch %d'%k] = v
        return status
    
    @_connected
    @_locked
    def Initialise(self):
        return (self.tm.Initialise())

    @_connected
    @_locked
    def EnableDLLFeedback(self):
        return rh(self.tm.EnableDLLFeedback())

    @_connected
    @_locked
    def PowerUpASIC(self,asic_id=0,individual=False):
        return  self.tm.PowerUpASIC(asic_id,individual)

    @_connected
    @_locked
    def SetHVDAC(self, superpixel,hval,switch_on=False):
        ''' Sets the HVDAC values for a specific superpixel '''
        rh(self.tm.SetHVDAC(superpixel,hval))
        if(switch_on):
            rh(self.tm.EnableHVSuperPixel(superpixel))
    
    @_connected
    @_locked
    def DisableHVSuperPixel(self, superpixel):
        rh(self.tm.DisableHVSuperPixel(superpixel))
    
    @_connected
    @_locked
    def ReadHVDAC(self):
        hvdac_values = dict()
        for channel in range (0,16) :
            # time.sleep(0.1)
            ret, answer = rh(self.tm.ReadSetting("HV{}_Voltage".format(channel)))
            if answer < 0x8000 :
                answer = answer + 0x8000
            else :
                answer = answer & 0x7FFF
            hvdac_values[channel] = answer *0.03815*2*20/1000
        return hvdac_values
    
    @_connected
    @_locked
    def WhichEnabled(self):
        enabled_channels = dict()
        for i in range(16):
            enabled_channels[i] = rh(self.tm.IsHVEnabled(i))[1]
            # time.sleep(0.1)
        return enabled_channels
    
    @_connected
    @_locked
    def Verbose(self,verb = True):
         self.tm.SetVerbose(verb)
    
    @_connected
    @_locked
    def PrepareSlowSignalDataTaking(self):
        rh(self.tm.WriteSetting("SlowADCEnable_Primary",1))
        rh(self.tm.WriteSetting("SlowADCEnable_Aux",1))
        rh(self.tm.WriteSetting("SlowADCEnable_Power",1))
        self.prepared_ssm =True
    
    @_connected
    @_locked    
    def PrepareFastSignalDataTaking(self):
        for asic in range(4):
            rh(
                self.WriteSetting("EnableChannelsASIC{}".format(asic), 0xffff)
            )
        rh(self.tm.WriteSetting("Zero_Enable", 0x1))
        rh(self.tm.WriteSetting("DoneSignalSpeedUp",0))
        nblocks = 8
        rh(self.tm.WriteSetting("NumberOfBlocks", nblocks-1))
        rh(self.tm.WriteSetting("MaxChannelsInPacket", 4))
        #lookback time between instant when the trigger is 
        #issued and the portion of the ASIC storage to digitize
        rh(self.tm.WriteSetting("TriggerDelay", 500)) 
        rh(self.tm.WriteSetting("TACK_TriggerType", 0x0))
        rh(self.tm.WriteSetting("TACK_TriggerMode", 0x0))
        rh(self.tm.WriteSetting("ExtTriggerDirection", 0x1)) # hardsync
    
    
    @_connected
    @_locked
    def GoToSafe(self):
        return rh(self.tm.GoToSafe())

    
    @_connected
    @_locked
    def GoToReady(self):
        return rh(self.tm.GoToReady())

    
    @_connected
    @_locked
    def GoToPreSync(self):
        return rh(self.tm.GoToPreSync())

    @_connected    
    @_locked
    def TestLocked(self):
        print('Testing locked')
        time.sleep(5)
