import zmq

from multiprocessing import Process, Lock
import multiprocessing
import threading
from multiprocessing import managers
import pickle

import time

from tm_server.utils import msg

class MyManager(managers.SyncManager):
    pass

SYNC_MANAGER = MyManager(address=("localhost",22229),authkey=bytearray("12345",'utf8'))
MSG = 'ALIVE'

LAST_PORT_NR = 8201
def AcquireTM(context=None):
    from tm_server import TMWrapper
    global LAST_PORT_NR
    if(context == None):
        context = zmq.Context()
    else:
        context = context

    worker = context.socket(zmq.REQ)
    worker.connect('ipc:///tmp/tm_dist')

    mgr = SYNC_MANAGER
    mgr.connect()
    multiprocessing.current_process().authkey = bytearray("12345",'utf8')
    worker.send_pyobj('AQ_TM')
    tm_sync_vars,tm_config = worker.recv_pyobj()
    while(True):
        LAST_PORT_NR +=1
        tm_config['host_port'] = LAST_PORT_NR
        tm = TMWrapper(sync_variables =tm_sync_vars, reconnect = True ,**tm_config)
        if(tm.connection_valid):
            break
    return tm


class TMDistributor(multiprocessing.Process):
    """TMDistributor service"""
    def __init__(self,tm_sync_vars,tm_config):
        super(TMDistributor, self).__init__()
        self.tm_sync_vars = tm_sync_vars
        self.tm_config = tm_config
        self.name = 'TM-Distributor-Service'
    def init(self):
        from .log_server import Logger,log_server
        self.context = zmq.Context()
        self.log = Logger(self.context,'TMDistributor')
        time.sleep(0.5)
        self.log('TMDistributor Initializing')
           
    def run(self):
        self.init()
        distr_socket = self.context.socket(zmq.REP)
        distr_socket.bind('ipc:///tmp/tm_dist')
        self.log('TMDistributor started')
        while True:
            msg = distr_socket.recv_pyobj()
            if(msg == 'CLOSE'):
                distr_socket.send_pyobj("Stopping...")
                break
            elif(msg == 'AQ_TM'):
                distr_socket.send_pyobj((self.tm_sync_vars,self.tm_config))
                self.log('Distributed TMWrapper')
            else:
                distr_socket.send_pyobj("Invalid request.")
