import zmq

from multiprocessing import Process, Lock
import multiprocessing
import threading
from multiprocessing import managers
import pickle
import time

from tm_server.utils import msg
from .log_server import Logger,log_server
from .monitor_server import MonitorService
from .distributor_server import TMDistributor, SYNC_MANAGER
from tm_server import TMWrapper

class TMServer():
    """ServerTask"""
    def __init__(self,tm_config):
        print('Initializing...')
        self.services = []
        self.tm_config = tm_config
        #Getting zmq context and starting logging server
        self.context = zmq.Context()
        self.serv_logging = Process(target=log_server,name='Logging-Service')
        self.serv_logging.start()
        time.sleep(0.5)
        
        self.log = Logger(self.context,name='TMServer')
        
        self.log('Setting up SyncManager server...')
        self.mgr = SYNC_MANAGER 
        self.mgr.start()
        

        self.config = tm_config    
        self.lock = self.mgr.Lock()
        #self.connected = self.mgr.Value('i',0)

        

    def run(self):
        self.log('Initializing target interface')
        self.tm = TMWrapper(**self.tm_config['tm_config'],sync_variables=[self.lock])
        # self.tm.connected = True
        import os
        print('TM-server pid',os.getpid())
        if(self.tm.connected):
            print('HERE')
            self.log('Setting up heartbeat server')
            self.serv_heartbeat = Process(target=tm_heartbeat, name = 'Heartbeat-Service',args=(self.tm,))
            
            self.log('Setting up TM distributor server')
            self.serv_tm_distr = TMDistributor((self.lock,'ipc:///tmp/heartbeat'), self.tm_config['tm_config'])
            
            self.log('Setting up monitor server')
            self.serv_monitor = MonitorService()
            
            self.services.append(self.serv_tm_distr)
            self.services.append(self.serv_heartbeat)        
            self.services.append(self.serv_logging)
            self.services.append(self.serv_monitor)

            global MSG
            self.log('Starting main server loop')
            self.serv_heartbeat.start()
            self.serv_tm_distr.start()
            self.serv_monitor.start()
            
            ins = input('Press enter')
            while(ins != 'x'):
                ins = input('Set MSG: ')
                MSG = ins
            self.tm.tm.CloseSockets()
        else:
            self.services.append(self.serv_logging)
            print('Failed to connect')

        #self.connected.value = 0
        for s in self.services:
            self.log("Terminating service %s"%s.name)
            print("Terminating service %s"%s.name)
            time.sleep(0.5)
            s.terminate()
            s.join()

def tm_heartbeat_sim(tm):
    global MSG
    context = zmq.Context()
    hb_socket = context.socket(zmq.PUB)
    hb_socket.bind('ipc:///tmp/heartbeat')
    while(True):
        print('Sending ',MSG)
        hb_socket.send_pyobj(MSG)
        time.sleep(2)

def tm_heartbeat(tm):
    context = zmq.Context()
    hb_socket = context.socket(zmq.PUB)
    hb_socket.bind('ipc:///tmp/heartbeat')
    log = Logger(context,name='Heartbeat')
    lost_connection = False
    while(True):
        ans = tm.GetFirmwareVersion(_lock=False)
        if(ans[0]!=0):
            hb_socket.send_pyobj('DEAD')
            log(msg.colr('rf','Warning: Lost connection to TM'))
            lost_connection = True
        else:
            if(lost_connection):
                log(msg.colr('g','Regained connection to TM'))
                lost_connection = False

            hb_socket.send_pyobj('ALIVE')
        time.sleep(2)
    hb_socket.close()
    context.term()





     