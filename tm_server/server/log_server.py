import zmq
from multiprocessing import Process, Lock
import multiprocessing
import threading
from multiprocessing import managers
import pickle
from tm_server.utils import msg as m
import sys
from collections import deque

class Logger(object):
    def __init__(self,context=None, name='',print = False):
        if(context==None):
           context = zmq.Context()
        self.log_in = context.socket(zmq.PUB)
        self.name = sys._getframe().f_back.f_back.f_code.co_name+': '+ name
        self.log_in.connect('ipc:///tmp/tm_log_in')
        self.print = print
    def __call__(self,msg):
        if(self.print):
            print(msg)
        self.log_in.send(("%s: %s"%(m.colr('b',self.name),msg)).encode('ascii'))

LOG_BUFFER =deque()

def log_server(buffer_size=200):
    context = zmq.Context()
    log_in = context.socket(zmq.SUB)
    log_in.setsockopt(zmq.SUBSCRIBE, b'')
    log_in.bind('ipc:///tmp/tm_log_in')
    log_out = context.socket(zmq.PUB)
    log_out.bind('ipc:///tmp/tm_log_out')
    log_history_thread = threading.Thread(target = log_hist_req_thread, args = (context, ))
    log_history_thread.start()
    while(True):
        msg = log_in.recv()
        log_out.send(msg)
        LOG_BUFFER.append(msg)
        if(len(LOG_BUFFER)>buffer_size):
            LOG_BUFFER.popleft()
    log_in.close()
    log_out.close()
    context.term()

def log_hist_req_thread(context):
    log_hist = context.socket(zmq.REP)
    log_hist.bind('ipc:///tmp/tm_log_hist')
    while(True):
        msg = log_hist.recv_pyobj()
        if(msg == "GET"):
            log_hist.send_pyobj(LOG_BUFFER)
        else:
            log_hist.send_pyobj('Unknown request')

def log_client():
    context = zmq.Context()
    log_out = context.socket(zmq.SUB)
    log_out.connect('ipc:///tmp/tm_log_out')
    log_out.setsockopt(zmq.SUBSCRIBE, b'')
    log_hist = context.socket(zmq.REQ)
    log_hist.connect('ipc:///tmp/tm_log_hist')
    log_hist.send_pyobj("GET")
    history = log_hist.recv_pyobj()
    for l in history:
        print(l.decode('ascii'))
    while(True):
        msg = log_out.recv()
        print(msg.decode("utf-8")) 