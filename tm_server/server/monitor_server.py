import sqlite3 as lite
import os
import numpy as np
from numpy.random import uniform as ru
import time
import datetime
import numpy as np
# import pyqtgraph as pg
import sqlite3 

import random
import time
import numpy as np
import collections
import socket
import threading
import math
from datetime import datetime

import multiprocessing
import zmq
from .log_server import Logger


class MonitorService(multiprocessing.Process):
    def __init__(self,tm=None, interval=2.0):
        super(MonitorService, self).__init__()
        self.interval = interval    
         
        #t = datetime.datetime.now()#%s.db'%d.strftime("%Y-%m-%d_%H:%M")
    def _init(self):
        path = './'#os.path.dirname(os.path.abspath(__file__))
        self.con = lite.connect(path+'/monitor.db', check_same_thread=False,detect_types=sqlite3.PARSE_DECLTYPES)
        


        with self.con:
            cur = self.con.cursor()
            cur.execute("SELECT name FROM sqlite_master WHERE type='table';")
            l = cur.fetchall()
            #check if the TM-Monitor table is in the database
            #otherwise create it
            if(len(l)==0 or 'TMMonitor' not in l[0]):
                dacvalsdef = "".join([',dac_suppix_%d REAL'%i for i in range(16)])
                cur.execute('''CREATE TABLE TMMonitor(id INTEGER PRIMARY KEY, 
                                time timestamp,
                                hv_current REAL,
                                hv_voltage REAL,
                                temp_powb REAL,
                                temp_auxb REAL,
                                temp_primb REAL,
                                temp_sipm REAL
                                %s);'''%dacvalsdef)
            self.dbvals = np.array([ru(0.1,0.5),ru(50,80),ru(15,25),ru(15,25),ru(15,25),ru(15,25)])


    def run(self):
        from .distributor_server import AcquireTM
        self._init()

        self.context = zmq.Context()
        self.log = Logger(self.context,'Monitor Service')
        time.sleep(0.5)
        self.log('MonitorService Initializing')
        self.tm = AcquireTM()
        # print('here')
        while(True):
            self.monitor()
            time.sleep(self.interval)


    def monitor(self):
        # print('here') 
        if(self.tm == None):     
            self.dbvals += ru(-5,5,6)
        else:
            self.log('connected %s'%self.tm.connected)
            if(not self.tm.connected):
                return

            mv = self.tm.monitor()
            self.dbvals = []
            dbfield_names_list = []

            for k,v in mv.items():
                self.dbvals.append(v)
                dbfield_names_list.append(",%s"%k) 

        self.dbfield_names = "".join(dbfield_names_list)
        self.dbnfields = "".join([",?" for i in range(len(self.dbvals))])
        with self.con:
          cur = self.con.cursor()
          cur.execute(''' INSERT INTO 
                              TMMonitor(time %s) 
                          VALUES 
                              (?%s);'''%(self.dbfield_names,self.dbnfields),
                          tuple([datetime.now()]+list(self.dbvals)))